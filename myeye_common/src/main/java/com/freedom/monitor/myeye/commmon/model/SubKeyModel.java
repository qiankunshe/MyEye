package com.freedom.monitor.myeye.commmon.model;

public class SubKeyModel {
	private String sk;// subkey
	private long tc = 0;// total count
	private long ts = 0;// total Succeed
	private long tct = 0;// total cost time
	private long maxct = 0;// max cost time
	private long minct = 0;// min cost time
	
	public SubKeyModel(String subKey,long totalCount,long totalSucceed,long totalCostTime,long maxCostTime,long minCostTime){
		sk=subKey;
		tc=totalCount;
		ts=totalSucceed;
		tct=totalCostTime;
		maxct=maxCostTime;
		minct=minCostTime;
	}
	public String getSk() {
		return sk;
	}
	public void setSk(String sk) {
		this.sk = sk;
	}
	public long getTc() {
		return tc;
	}
	public void setTc(long tc) {
		this.tc = tc;
	}
	public long getTs() {
		return ts;
	}
	public void setTs(long ts) {
		this.ts = ts;
	}
	public long getTct() {
		return tct;
	}
	public void setTct(long tct) {
		this.tct = tct;
	}
	public long getMaxct() {
		return maxct;
	}
	public void setMaxct(long maxct) {
		this.maxct = maxct;
	}
	public long getMinct() {
		return minct;
	}
	public void setMinct(long minct) {
		this.minct = minct;
	}
	
	
}
