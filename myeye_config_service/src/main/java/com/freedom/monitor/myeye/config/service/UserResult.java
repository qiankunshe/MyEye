/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.freedom.monitor.myeye.config.service;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2017-01-03")
public class UserResult implements org.apache.thrift.TBase<UserResult, UserResult._Fields>, java.io.Serializable, Cloneable, Comparable<UserResult> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("UserResult");

  private static final org.apache.thrift.protocol.TField USER_LIST_FIELD_DESC = new org.apache.thrift.protocol.TField("userList", org.apache.thrift.protocol.TType.LIST, (short)1);
  private static final org.apache.thrift.protocol.TField TOTAL_FIELD_DESC = new org.apache.thrift.protocol.TField("total", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField SUCCEED_FIELD_DESC = new org.apache.thrift.protocol.TField("succeed", org.apache.thrift.protocol.TType.BOOL, (short)3);
  private static final org.apache.thrift.protocol.TField ERROR_MSG_FIELD_DESC = new org.apache.thrift.protocol.TField("errorMsg", org.apache.thrift.protocol.TType.STRING, (short)4);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new UserResultStandardSchemeFactory());
    schemes.put(TupleScheme.class, new UserResultTupleSchemeFactory());
  }

  public List<User> userList; // required
  public int total; // required
  public boolean succeed; // required
  public String errorMsg; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    USER_LIST((short)1, "userList"),
    TOTAL((short)2, "total"),
    SUCCEED((short)3, "succeed"),
    ERROR_MSG((short)4, "errorMsg");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // USER_LIST
          return USER_LIST;
        case 2: // TOTAL
          return TOTAL;
        case 3: // SUCCEED
          return SUCCEED;
        case 4: // ERROR_MSG
          return ERROR_MSG;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __TOTAL_ISSET_ID = 0;
  private static final int __SUCCEED_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.USER_LIST, new org.apache.thrift.meta_data.FieldMetaData("userList", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, User.class))));
    tmpMap.put(_Fields.TOTAL, new org.apache.thrift.meta_data.FieldMetaData("total", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.SUCCEED, new org.apache.thrift.meta_data.FieldMetaData("succeed", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.BOOL)));
    tmpMap.put(_Fields.ERROR_MSG, new org.apache.thrift.meta_data.FieldMetaData("errorMsg", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(UserResult.class, metaDataMap);
  }

  public UserResult() {
  }

  public UserResult(
    List<User> userList,
    int total,
    boolean succeed,
    String errorMsg)
  {
    this();
    this.userList = userList;
    this.total = total;
    setTotalIsSet(true);
    this.succeed = succeed;
    setSucceedIsSet(true);
    this.errorMsg = errorMsg;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public UserResult(UserResult other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetUserList()) {
      List<User> __this__userList = new ArrayList<User>(other.userList.size());
      for (User other_element : other.userList) {
        __this__userList.add(new User(other_element));
      }
      this.userList = __this__userList;
    }
    this.total = other.total;
    this.succeed = other.succeed;
    if (other.isSetErrorMsg()) {
      this.errorMsg = other.errorMsg;
    }
  }

  public UserResult deepCopy() {
    return new UserResult(this);
  }

  @Override
  public void clear() {
    this.userList = null;
    setTotalIsSet(false);
    this.total = 0;
    setSucceedIsSet(false);
    this.succeed = false;
    this.errorMsg = null;
  }

  public int getUserListSize() {
    return (this.userList == null) ? 0 : this.userList.size();
  }

  public java.util.Iterator<User> getUserListIterator() {
    return (this.userList == null) ? null : this.userList.iterator();
  }

  public void addToUserList(User elem) {
    if (this.userList == null) {
      this.userList = new ArrayList<User>();
    }
    this.userList.add(elem);
  }

  public List<User> getUserList() {
    return this.userList;
  }

  public UserResult setUserList(List<User> userList) {
    this.userList = userList;
    return this;
  }

  public void unsetUserList() {
    this.userList = null;
  }

  /** Returns true if field userList is set (has been assigned a value) and false otherwise */
  public boolean isSetUserList() {
    return this.userList != null;
  }

  public void setUserListIsSet(boolean value) {
    if (!value) {
      this.userList = null;
    }
  }

  public int getTotal() {
    return this.total;
  }

  public UserResult setTotal(int total) {
    this.total = total;
    setTotalIsSet(true);
    return this;
  }

  public void unsetTotal() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __TOTAL_ISSET_ID);
  }

  /** Returns true if field total is set (has been assigned a value) and false otherwise */
  public boolean isSetTotal() {
    return EncodingUtils.testBit(__isset_bitfield, __TOTAL_ISSET_ID);
  }

  public void setTotalIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __TOTAL_ISSET_ID, value);
  }

  public boolean isSucceed() {
    return this.succeed;
  }

  public UserResult setSucceed(boolean succeed) {
    this.succeed = succeed;
    setSucceedIsSet(true);
    return this;
  }

  public void unsetSucceed() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __SUCCEED_ISSET_ID);
  }

  /** Returns true if field succeed is set (has been assigned a value) and false otherwise */
  public boolean isSetSucceed() {
    return EncodingUtils.testBit(__isset_bitfield, __SUCCEED_ISSET_ID);
  }

  public void setSucceedIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __SUCCEED_ISSET_ID, value);
  }

  public String getErrorMsg() {
    return this.errorMsg;
  }

  public UserResult setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
    return this;
  }

  public void unsetErrorMsg() {
    this.errorMsg = null;
  }

  /** Returns true if field errorMsg is set (has been assigned a value) and false otherwise */
  public boolean isSetErrorMsg() {
    return this.errorMsg != null;
  }

  public void setErrorMsgIsSet(boolean value) {
    if (!value) {
      this.errorMsg = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case USER_LIST:
      if (value == null) {
        unsetUserList();
      } else {
        setUserList((List<User>)value);
      }
      break;

    case TOTAL:
      if (value == null) {
        unsetTotal();
      } else {
        setTotal((Integer)value);
      }
      break;

    case SUCCEED:
      if (value == null) {
        unsetSucceed();
      } else {
        setSucceed((Boolean)value);
      }
      break;

    case ERROR_MSG:
      if (value == null) {
        unsetErrorMsg();
      } else {
        setErrorMsg((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case USER_LIST:
      return getUserList();

    case TOTAL:
      return getTotal();

    case SUCCEED:
      return isSucceed();

    case ERROR_MSG:
      return getErrorMsg();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case USER_LIST:
      return isSetUserList();
    case TOTAL:
      return isSetTotal();
    case SUCCEED:
      return isSetSucceed();
    case ERROR_MSG:
      return isSetErrorMsg();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof UserResult)
      return this.equals((UserResult)that);
    return false;
  }

  public boolean equals(UserResult that) {
    if (that == null)
      return false;

    boolean this_present_userList = true && this.isSetUserList();
    boolean that_present_userList = true && that.isSetUserList();
    if (this_present_userList || that_present_userList) {
      if (!(this_present_userList && that_present_userList))
        return false;
      if (!this.userList.equals(that.userList))
        return false;
    }

    boolean this_present_total = true;
    boolean that_present_total = true;
    if (this_present_total || that_present_total) {
      if (!(this_present_total && that_present_total))
        return false;
      if (this.total != that.total)
        return false;
    }

    boolean this_present_succeed = true;
    boolean that_present_succeed = true;
    if (this_present_succeed || that_present_succeed) {
      if (!(this_present_succeed && that_present_succeed))
        return false;
      if (this.succeed != that.succeed)
        return false;
    }

    boolean this_present_errorMsg = true && this.isSetErrorMsg();
    boolean that_present_errorMsg = true && that.isSetErrorMsg();
    if (this_present_errorMsg || that_present_errorMsg) {
      if (!(this_present_errorMsg && that_present_errorMsg))
        return false;
      if (!this.errorMsg.equals(that.errorMsg))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_userList = true && (isSetUserList());
    list.add(present_userList);
    if (present_userList)
      list.add(userList);

    boolean present_total = true;
    list.add(present_total);
    if (present_total)
      list.add(total);

    boolean present_succeed = true;
    list.add(present_succeed);
    if (present_succeed)
      list.add(succeed);

    boolean present_errorMsg = true && (isSetErrorMsg());
    list.add(present_errorMsg);
    if (present_errorMsg)
      list.add(errorMsg);

    return list.hashCode();
  }

  @Override
  public int compareTo(UserResult other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetUserList()).compareTo(other.isSetUserList());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUserList()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.userList, other.userList);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetTotal()).compareTo(other.isSetTotal());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetTotal()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.total, other.total);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetSucceed()).compareTo(other.isSetSucceed());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetSucceed()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.succeed, other.succeed);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetErrorMsg()).compareTo(other.isSetErrorMsg());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetErrorMsg()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.errorMsg, other.errorMsg);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("UserResult(");
    boolean first = true;

    sb.append("userList:");
    if (this.userList == null) {
      sb.append("null");
    } else {
      sb.append(this.userList);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("total:");
    sb.append(this.total);
    first = false;
    if (!first) sb.append(", ");
    sb.append("succeed:");
    sb.append(this.succeed);
    first = false;
    if (!first) sb.append(", ");
    sb.append("errorMsg:");
    if (this.errorMsg == null) {
      sb.append("null");
    } else {
      sb.append(this.errorMsg);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class UserResultStandardSchemeFactory implements SchemeFactory {
    public UserResultStandardScheme getScheme() {
      return new UserResultStandardScheme();
    }
  }

  private static class UserResultStandardScheme extends StandardScheme<UserResult> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, UserResult struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // USER_LIST
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list0 = iprot.readListBegin();
                struct.userList = new ArrayList<User>(_list0.size);
                User _elem1;
                for (int _i2 = 0; _i2 < _list0.size; ++_i2)
                {
                  _elem1 = new User();
                  _elem1.read(iprot);
                  struct.userList.add(_elem1);
                }
                iprot.readListEnd();
              }
              struct.setUserListIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // TOTAL
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.total = iprot.readI32();
              struct.setTotalIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // SUCCEED
            if (schemeField.type == org.apache.thrift.protocol.TType.BOOL) {
              struct.succeed = iprot.readBool();
              struct.setSucceedIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // ERROR_MSG
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.errorMsg = iprot.readString();
              struct.setErrorMsgIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, UserResult struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.userList != null) {
        oprot.writeFieldBegin(USER_LIST_FIELD_DESC);
        {
          oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, struct.userList.size()));
          for (User _iter3 : struct.userList)
          {
            _iter3.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(TOTAL_FIELD_DESC);
      oprot.writeI32(struct.total);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SUCCEED_FIELD_DESC);
      oprot.writeBool(struct.succeed);
      oprot.writeFieldEnd();
      if (struct.errorMsg != null) {
        oprot.writeFieldBegin(ERROR_MSG_FIELD_DESC);
        oprot.writeString(struct.errorMsg);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class UserResultTupleSchemeFactory implements SchemeFactory {
    public UserResultTupleScheme getScheme() {
      return new UserResultTupleScheme();
    }
  }

  private static class UserResultTupleScheme extends TupleScheme<UserResult> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, UserResult struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetUserList()) {
        optionals.set(0);
      }
      if (struct.isSetTotal()) {
        optionals.set(1);
      }
      if (struct.isSetSucceed()) {
        optionals.set(2);
      }
      if (struct.isSetErrorMsg()) {
        optionals.set(3);
      }
      oprot.writeBitSet(optionals, 4);
      if (struct.isSetUserList()) {
        {
          oprot.writeI32(struct.userList.size());
          for (User _iter4 : struct.userList)
          {
            _iter4.write(oprot);
          }
        }
      }
      if (struct.isSetTotal()) {
        oprot.writeI32(struct.total);
      }
      if (struct.isSetSucceed()) {
        oprot.writeBool(struct.succeed);
      }
      if (struct.isSetErrorMsg()) {
        oprot.writeString(struct.errorMsg);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, UserResult struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(4);
      if (incoming.get(0)) {
        {
          org.apache.thrift.protocol.TList _list5 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, iprot.readI32());
          struct.userList = new ArrayList<User>(_list5.size);
          User _elem6;
          for (int _i7 = 0; _i7 < _list5.size; ++_i7)
          {
            _elem6 = new User();
            _elem6.read(iprot);
            struct.userList.add(_elem6);
          }
        }
        struct.setUserListIsSet(true);
      }
      if (incoming.get(1)) {
        struct.total = iprot.readI32();
        struct.setTotalIsSet(true);
      }
      if (incoming.get(2)) {
        struct.succeed = iprot.readBool();
        struct.setSucceedIsSet(true);
      }
      if (incoming.get(3)) {
        struct.errorMsg = iprot.readString();
        struct.setErrorMsgIsSet(true);
      }
    }
  }

}

