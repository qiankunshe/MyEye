namespace java com.freedom.monitor.myeye.config.service




struct Member{
 1: i64 id,
 2: string userid,
 3: string username,
 4: string name,
 5: string pwd,
 6: i32 role,
 7: string roleName,
 8: string createTime,
 9: string token,
 10:string createBy
}

struct MemberResult{
 1: list<Member> memberList,
 2: i32 total,
 3: bool succeed,
 4: string errorMsg
}


service MemberConfigService {


 MemberResult queryMember(1:i32 page,2:i32 rows,3:string operatorId)
 bool         deleteMember(1:string userid)
 bool         addMember(1:string username,2: string name,3: string pwd,4:i32 role,5:string createBy)
 bool         editMember(1:string username,2:string name,3:string pwd,4:i32 role,5:string userid)


}

