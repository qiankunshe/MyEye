namespace java com.freedom.monitor.myeye.config.service



struct ServiceTemplate{
 1: i64 id,
 2: string serviceId,
 3: string serviceName,
 4: string serviceCode,
 5: string desc,
 6: string createTime,
 7: i32 totalCount,
 8: i32 thresholdCount,
 9: i32 succeedRatioThreshold,
 10:i32 averageTimeCostThreshold,
 11:i32 maxTimeCostThreshold,
 12:i32 minTimeCostThreshold,
 13:i32 maxInvokedCount,
 14:i32 minInvokedCount,
 15:i32 alarmPeriod
}

struct ServiceTemplateResult{
 1: list<ServiceTemplate> serviceTemplateList,
 2: i32 total,
 3: bool succeed,
 4: string errorMsg
}

service ServiceTemplateConfigService {

 
 ServiceTemplateResult queryServiceTemplate(1:i32 page,2:i32 rows)
 bool                  addServiceTemplate(1:string serviceName,2:string serviceCode,3:i32 totalCount,4:i32 thresholdCount,5:i32 alarmPeriod,6:i32 succeedRatioThreshold,7:i32 averageTimeCostThreshold,8:i32 maxTimeCostThreshold,9:i32 minTimeCostThreshold,10:string desc,11:i32 maxInvokedCount,12:i32 minInvokedCount)
 bool                  editServiceTemplate(1:string serviceName,2:string serviceCode,3:i32 totalCount,4:i32 thresholdCount,5:i32 alarmPeriod,6:i32 succeedRatioThreshold,7:i32 averageTimeCostThreshold,8:i32 maxTimeCostThreshold,9:i32 minTimeCostThreshold,10:string desc,11:string serviceId,12:i32 maxInvokedCount,13:i32 minInvokedCount)
 bool                  deleteServiceTemplate(1:string serviceId)
 
 
}

