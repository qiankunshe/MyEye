package com.freedom.monitor.myeye.config.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.util.ResourceUtils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.freedom.rpc.thrift.common.utils.Logger;

public class DruidUtils {
	private static DruidDataSource instance = null;
	private static final Logger logger = Logger.getLogger(DruidUtils.class);
	private static String DRUID_CONFIG_FILE = null;
	static {
		// 0)判断应该读哪个文件
		if (null != System.getProperty("serverProperties")) {
			DRUID_CONFIG_FILE = System.getProperty("serverProperties");
		} else {
			try {
				if (ResourceUtils.getFile("classpath:server.properties").exists()) {
					DRUID_CONFIG_FILE = "classpath:server.properties";
				} else {
					throw new Exception("server.properties not exist...");
				}
			} catch (Exception e) {// windows,eclipse
				DRUID_CONFIG_FILE = "src/main/resources/server.properties";
			}
		}
		logger.info("succeed to select " + DRUID_CONFIG_FILE);
		// 1)获取 属性对象
		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(ResourceUtils.getFile(DRUID_CONFIG_FILE)));
			// Thread.currentThread().getContextClassLoader().getResourceAsStream(MyConstants.CONFIG_FILE);
			props.load(in);
			in.close();
		} catch (Exception e) {
			logger.error("fail to read config file " + DRUID_CONFIG_FILE);
			System.exit(-1);
		}
		logger.info("succeed to read " + DRUID_CONFIG_FILE);
		// 2)获取url
		String url = props.getProperty("druid.url");
		// 3)获取username
		String username = props.getProperty("druid.username");
		// 4)获取password
		String password = props.getProperty("druid.password");
		// 5)获取driver
		String driver = props.getProperty("druid.driver");
		// 6)获取initial size
		String initialSize = props.getProperty("druid.initialSize");
		// 7)设置minIdle
		String minIdle = props.getProperty("druid.minIdle");
		// 8)设置maxActive
		String maxActive = props.getProperty("druid.maxActive");
		// 9)设置maxWait
		String maxWait = props.getProperty("druid.maxWait");
		// 10)设置timeBetweenEvictionRunsMillis
		String timeBetweenEviction = props.getProperty("druid.timeBetweenEvictionRunsMillis");
		// 11)设置 minEvictableIdleTimeMillis
		String minEvictableIdleTimeMillis = props.getProperty("druid.minEvictableIdleTimeMillis");
		// 12)获取validationQuery
		String validationQuery = props.getProperty("druid.validationQuery");
		// 13)获取testWhileIdle
		String testWhileIdle = props.getProperty("druid.testWhileIdle");
		// 14) 获取testOnBorrow
		String testOnBorrow = props.getProperty("druid.testOnBorrow");
		// 15)获取testOnReturn
		String testOnReturn = props.getProperty("druid.testOnReturn");
		// 16)获取init
		String init = props.getProperty("druid.init");
		// 17)初始化map
		logger.info("begin to build map");
		Map<String, String> map = new HashMap<String, String>();
		map.put(DruidDataSourceFactory.PROP_URL, url);
		map.put(DruidDataSourceFactory.PROP_USERNAME, username);
		map.put(DruidDataSourceFactory.PROP_PASSWORD, password);
		map.put(DruidDataSourceFactory.PROP_DRIVERCLASSNAME, driver);
		map.put(DruidDataSourceFactory.PROP_INITIALSIZE, initialSize);
		map.put(DruidDataSourceFactory.PROP_MINIDLE, minIdle);
		map.put(DruidDataSourceFactory.PROP_MAXACTIVE, maxActive);
		map.put(DruidDataSourceFactory.PROP_MAXWAIT, maxWait);
		map.put(DruidDataSourceFactory.PROP_TIMEBETWEENEVICTIONRUNSMILLIS, timeBetweenEviction);
		map.put(DruidDataSourceFactory.PROP_MINEVICTABLEIDLETIMEMILLIS, minEvictableIdleTimeMillis);
		map.put(DruidDataSourceFactory.PROP_VALIDATIONQUERY, validationQuery);
		map.put(DruidDataSourceFactory.PROP_TESTWHILEIDLE, testWhileIdle);
		map.put(DruidDataSourceFactory.PROP_TESTONBORROW, testOnBorrow);
		map.put(DruidDataSourceFactory.PROP_TESTONRETURN, testOnReturn);
		map.put(DruidDataSourceFactory.PROP_INIT, init);
		logger.info(map.toString());
		logger.info("succeed to build map");
		// 18)创建druid 连接池 对象
		try {
			instance = (DruidDataSource) DruidDataSourceFactory.createDataSource(map);
			logger.info("build druid instance end. instance->" + instance);
		} catch (Exception e) {
			logger.error("system error due to " + e.toString());
		}
		logger.info("build druid instance end. instance->" + instance);
	}

	public static DruidDataSource getInstance() {
		return instance;
	}
}
