package com.freedom.monitor.myeye.client.utils;

/**
 * 
 * @author zhiqiang.liu
 * @2016年1月1日
 *
 */
public class Logger {
	//
	private org.apache.log4j.Logger logger = null;

	private Logger(@SuppressWarnings("rawtypes") Class clazz) {
		// 你可以通过替换这里的具体实现，来自由切换成你的实际Log类
		logger = org.apache.log4j.LogManager.getLogger(clazz);
	}

	public void debug(String msg) {
		if (logger.isDebugEnabled()) {
			logger.debug(msg + " by " + Thread.currentThread().getName());
		}
	}

	public void info(String msg) {
		if (logger.isInfoEnabled()) {
			logger.info(msg + " by " + Thread.currentThread().getName());
		}
	}

	public void warn(String msg) {
		// 错误直接打
		logger.warn(msg + " by " + Thread.currentThread().getName());
	}

	public void error(String msg) {
		// 错误直接打
		logger.error(msg + " by " + Thread.currentThread().getName());
	}

	public void fatal(String msg) {
		// 错误直接打
		logger.fatal(msg + " by " + Thread.currentThread().getName());
	}

	public static Logger getLogger(@SuppressWarnings("rawtypes") Class clazz) {
		return new Logger(clazz);
	}
}
