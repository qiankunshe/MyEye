package com.freedom.monitor.myeye.client.upload;

import com.freedom.monitor.myeye.client.queue.DataQueue;
import com.freedom.monitor.myeye.client.utils.HttpUtils;
import com.freedom.monitor.myeye.client.utils.PropertyUtils;
import com.freedom.monitor.myeye.commmon.utils.StringUtils;

import okhttp3.OkHttpClient;

public class UploadRunnable implements Runnable {

	private String product = null;
	private String service = null;
	private String productServiceKey = null;
	private static String UPLOAD_URL = "reportUrl";
	private OkHttpClient client = new OkHttpClient();

	//
	public UploadRunnable(String p, String s) {
		product = p;
		service = s;
		productServiceKey = StringUtils.unionByMagicKey(p, s);
	}

	private void execute() {
		// 1)取出数据
		String data = DataQueue.getData(productServiceKey);
		if (null == data) {// 有这种可能性发生
			return;
		}
		// 2)使用OkHttp
		try {
			HttpUtils.post(client, PropertyUtils.getInstance().getProperty(UPLOAD_URL), data);
		} catch (Exception e) {

		}
	}

	@Override
	public void run() {
		try {
			// long begin = System.currentTimeMillis();
			execute();
			// long end = System.currentTimeMillis();
			// logger.debug("reap Runnable --->" + (end - begin) + " ms");
		} catch (Throwable t) {
		}
	}

}
