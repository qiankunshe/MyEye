package com.freedom.monitor.myeye.client.utils;

public class NumberUtils {
	public static long max(long a, long b) {
		return a >= b ? a : b;
	}

	public static long min(long a, long b) {
		return a <= b ? a : b;
	}

}
