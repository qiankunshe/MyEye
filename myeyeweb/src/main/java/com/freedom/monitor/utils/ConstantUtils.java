package com.freedom.monitor.utils;

public class ConstantUtils {
	// 用于返回错误提示窗口
	public static String ERROR_DIALOG_PREFIX = "<div id=\"errorDlg\" class=\"easyui-dialog\" title=\"友情提醒 \" data-options=\"iconCls:'icon-save'\" style=\"width:400px;height:200px;padding:10px\">";
	public static String ERROR_DIALOG_SUFFIX = "</div>"//
			+ "<script type=\"text/javascript\">"//
			+ "$('#errorDlg').dialog('open');"//
			+ "</script>";
	// 模式框
	public static String MODEL_WINDOW_PREFIX = ""//
			+ "<div id=\"w\" class=\"easyui-window\" title=\":)\" data-options=\"modal:true,closed:true,minimizable:false,maximizable:false,iconCls:'icon-save'\" style=\"width:500px;height:200px;padding:10px;\">"//
	;
	public static String MODEL_WINDOW_SUFFIX = "</div>";//
	public static String MODEL_WINDOW_SCRIPT = ""//
			+ "<script type=\"text/javascript\">"//
			+ "$(document).ready(function(){"//
			+ "$('#w').window('open');"//
			+ "}"//
			+ ");"//
			+ "</script>";

	// 用于存储cookie
	public static int COOKIE_MAX_AGE = -1;
	public static String COOKIE_USER_ID = "COOKIE_USER_ID";
	public static String COOKIE_TOKEN = "COOKIE_TOKEN";
	// 全局路径
	public static String GLOBAL_CONTEXT_PATH = "GLOBAL_CONTEXT_PATH";
}
