package com.freedom.monitor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.freedom.monitor.myeye.config.service.Product;
import com.freedom.monitor.myeye.config.service.ProductConfigService;
import com.freedom.monitor.myeye.config.service.ProductServiceConfigService;
import com.freedom.monitor.myeye.config.service.ServiceTemplateResult;
import com.freedom.monitor.utils.ConstantUtils;
import com.freedom.monitor.utils.CookieUtils;
import com.freedom.monitor.utils.ExclusionStrategyUtils;
import com.freedom.monitor.utils.StringUtils;
import com.freedom.rpc.thrift.common.utils.Logger;
import com.freedom.rpc.thrift.common.utils.SocketUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@RequestMapping(value = "/productService")
public class ProductServiceController {
	//
	private static final Logger logger = Logger.getLogger(ProductServiceController.class);

	// 首页进来
	@RequestMapping(value = "/index", method = { RequestMethod.GET })
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		logger.debug("ProductServiceController.index for query...");
		return new ModelAndView("productService/index", model);//
	}

	// 查询这个人，能看到哪些产品
	@RequestMapping(value = "/product", method = { RequestMethod.GET }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String product(HttpServletRequest request, HttpServletResponse response, ModelMap model) {// 系统进来时需要查询当前人可以看到的产品
		logger.info("ProductServiceController.product invoked...");
		try {
			// 1)先获取operatorId参数
			String operatorId = (String) CookieUtils.getCookie(request).get(ConstantUtils.COOKIE_USER_ID);
			if (false == StringUtils.valid(operatorId)) {
				throw new Exception("invalid userId,please re login...");
			}
			logger.debug("received userId--->" + operatorId);
			// 2)尝试获取远程服务结果
			logger.debug("try to get  productConfigServiceClient");
			ProductConfigService.Client client = (ProductConfigService.Client) SocketUtils
					.getSyncAopObject(ProductConfigService.Client.class);
			List<Product> productList = client.queryProductByOperatorId(operatorId);
			logger.debug("succeed to get ConfigService.Client configServiceClient");
			// 3)分析结果
			if (null != productList && productList.size() > 0) {
				// 正确且有数据
				// 先增加一个默认的
				Gson gson = new GsonBuilder().setExclusionStrategies(ExclusionStrategyUtils.getInstance()).create();
				String str = gson.toJson(productList);
				logger.debug("" + str);
				return str;
			}
			throw new Exception("query product result is not valid");
		} catch (Exception e) {
			return "[{\"pid\":\"\",\"pcode\":\"\",\"pname\":\"无产品可供选择\"}]";// 返回空数组
		}
		// 查询结束
	}

	// 查询所有服务-POST
	@RequestMapping(value = "/query", method = { RequestMethod.GET }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String queryPost(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		logger.info("Post -> ProductServiceController.query invoked...");
		try {
			// 1)先获取分页参数
			String page = request.getParameter("page");
			String rows = request.getParameter("rows");
			if (false == StringUtils.valid(page, rows)) {
				page = "1";
				rows = "10";
			}
			int intPage = Integer.parseInt(page.trim());
			int intRows = Integer.parseInt(rows.trim());
			logger.debug("received --->" + "page->" + page + " rows:" + rows);
			// 1.1)尝试获取productId
			String productId = request.getParameter("productId");
			if (false == StringUtils.valid(productId)) {
				logger.debug("no productId found,return empty result...");
				return "{\"total\":0,\"rows\":[]}";
			}
			// 2)尝试获取远程服务结果
			ServiceTemplateResult result = new ServiceTemplateResult();
			logger.debug("try to get ConfigService.Client configServiceClient");
			ProductServiceConfigService.Client configServiceClient = (ProductServiceConfigService.Client) SocketUtils
					.getSyncAopObject(ProductServiceConfigService.Client.class);
			logger.debug("succeed to get ConfigService.Client configServiceClient");
			result = configServiceClient.queryProductService(intPage, intRows, productId);					
			// 3)分析结果
			if (null != result && true == result.isSucceed() && null != result.getServiceTemplateList()
					&& result.getServiceTemplateListSize() >= 0) {
				// 正确且有数据
				Gson gson = new GsonBuilder().setExclusionStrategies(ExclusionStrategyUtils.getInstance()).create();
				String str = "" + "{\"total\":" + result.getTotal() + ","//
						+ "\"rows\":" + gson.toJson(result.getServiceTemplateList())//
						+ "}";
				logger.debug("" + str);
				return str;
			}
			throw new Exception("query product result is not valid");
		} catch (Exception e) {
			return "{\"total\":0,\"rows\":[]}";
		}
	}

	// 新增
	@RequestMapping(value = "/addOrUpdate", method = {
			RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String addOrUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		logger.debug("ProductServiceController.addOrUpdate() invoked...");
		try {
			// 1)获取参数
			String productId=request.getParameter("productId");
			String serviceId = request.getParameter("serviceId");
			String serviceName = request.getParameter("serviceName");
			String serviceCode = request.getParameter("serviceCode");
			String totalCount = request.getParameter("totalCount");
			String thresholdCount = request.getParameter("thresholdCount");
			String alarmPeriod = request.getParameter("alarmPeriod");
			String succeedRatioThreshold = request.getParameter("succeedRatioThreshold");
			String averageTimeCostThreshold = request.getParameter("averageTimeCostThreshold");
			String maxTimeCostThreshold = request.getParameter("maxTimeCostThreshold");
			String minTimeCostThreshold = request.getParameter("minTimeCostThreshold");
			String maxInvokedCount = request.getParameter("maxInvokedCount");
			String minInvokedCount = request.getParameter("minInvokedCount");
			String desc = request.getParameter("desc");
			if (false == StringUtils.valid(productId,serviceName, serviceCode, //
					totalCount, thresholdCount, alarmPeriod, //
					succeedRatioThreshold, averageTimeCostThreshold, maxTimeCostThreshold, minTimeCostThreshold, //
					maxInvokedCount, minInvokedCount)) {
				throw new Exception("invalid parameter...");
			}
			// 继续执行
			// 2)调用远程的程序
			logger.debug("try to get ConfigService.Client configServiceClient");
			ProductServiceConfigService.Client configServiceClient = (ProductServiceConfigService.Client) SocketUtils
					.getSyncAopObject(ProductServiceConfigService.Client.class);
			logger.debug("succeed to get ConfigService.Client configServiceClient");
			boolean result = false;
			if (null != serviceId && serviceId.trim().length() > 0) {
				logger.debug("action->edit");
				result = configServiceClient.editProductService(serviceName, serviceCode, Integer.parseInt(totalCount),
						Integer.parseInt(thresholdCount), Integer.parseInt(alarmPeriod),
						Integer.parseInt(succeedRatioThreshold), Integer.parseInt(averageTimeCostThreshold),
						Integer.parseInt(maxTimeCostThreshold), Integer.parseInt(minTimeCostThreshold), desc, serviceId,
						Integer.parseInt(maxInvokedCount), Integer.parseInt(minInvokedCount));
			} else {
				logger.debug("action->add");
				result = configServiceClient.addProductService(serviceName, serviceCode, Integer.parseInt(totalCount),
						Integer.parseInt(thresholdCount), Integer.parseInt(alarmPeriod),
						Integer.parseInt(succeedRatioThreshold), Integer.parseInt(averageTimeCostThreshold),
						Integer.parseInt(maxTimeCostThreshold), Integer.parseInt(minTimeCostThreshold), desc,
						Integer.parseInt(maxInvokedCount), Integer.parseInt(minInvokedCount),productId);
			}
			// 拿到了结果
			if (true == result) {
				// 正确且有数据
				return "{\"success\":true}";
			} else {
				return "{\"success\":false}";
			}
		} catch (Exception e) {
			return "{\"success\":false,\"errorMsg\":\"" + e.toString() + "\"}";
		}
	}

	// 删除
	@RequestMapping(value = "/del", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		logger.info("ProductServiceController.delete invoked...");
		boolean result = false;
		try {
			// 1)先获取id
			String serviceId = request.getParameter("serviceId");
			if (false == StringUtils.valid(serviceId)) {
				throw new Exception("invalid serviceId--->" + serviceId);
			}
			logger.debug("received to be delete serviceId --->" + serviceId);
			// 2)尝试获取远程服务结果
			logger.debug("try to get ConfigService.Client configServiceClient");
			ProductServiceConfigService.Client configServiceClient = (ProductServiceConfigService.Client) SocketUtils
					.getSyncAopObject(ProductServiceConfigService.Client.class);
			logger.debug("succeed to get ConfigService.Client configServiceClient");
			result = configServiceClient.deleteProductService(serviceId);
			// 拿到了结果
			if (true == result) {
				// 正确且有数据
				return "{\"success\":true}";

			} else {
				return "{\"success\":false}";
			}
		} catch (Exception e) {
			return "{\"success\":false,\"errorMsg\":\"" + e.toString() + "\"}";
		}
	}
}
