<%@ page language="java" session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 网页布局参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/complextoolbar.html# -->
<!-- 添加一行，参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/rowediting.html -->
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/esf/esf.css">

<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>
<body style="padding: 0px 0px 0px 0px; margin: 1px 1px 1px 1px;">

<!-- 新增窗口 结束-->
	<script type="text/javascript">
		var toolbar = [{
			text:'新增',
			iconCls:'icon-add',
			handler:function(){
				clearForm();
				$("#pid").val("");
				$('#newWindow').window('open');
			}
		},{
			text:'修改',
			iconCls:'icon-edit',
			handler:function(){
				clearForm();
				var row = $('#dg').datagrid('getSelected');  
	       		if (row){ 
	 				//把相关的值补上	 				
	 				$('#ff').form('load',row);
	 				$('#newWindow').window('open');
	       		}	     		 
				
			}
		},'-',{
			text:'删除',
			iconCls:'icon-remove',
			handler:function(){destroy();}
		}];	
		</script>
		
	<table id="dg" title="" class="easyui-datagrid"
		style="width: 98%; height: auto"
		data-options="pagination:true,fitcolumns:true,rownumbers:true,singleSelect:true,url:'<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/product/query',method:'get',toolbar:toolbar,remoteSort:false,
                multiSort:true">
		<thead>
			<tr>
				<th data-options="field:'id',width:40">ID</th>
				<th data-options="field:'pid',width:80">系统编码</th>
				<th	data-options="field:'pname',width:100,align:'center',editor:'textbox'">产品名称</th>
				<th	data-options="field:'pcode',width:100,align:'center',editor:'textbox'">产品编码</th>
				<th data-options="field:'pdesc',width:100,align:'center'">描述</th>
				<th data-options="field:'createTime',width:150,align:'center'">创建时间</th>
				<th data-options="field:'email',width:140,align:'center'">邮箱</th>
				<th data-options="field:'mobile',width:140,align:'center'">手机号</th>				
				<th data-options="field:'createBy',width:100,align:'center'">创建者编码</th>
			</tr>
		</thead>
	</table>
	
	
	
	
<!-- 新增/修改 窗口 开始 -->
	<div id="newWindow" class="easyui-window" title="编辑" data-options="modal:true,closed:true,minimizable:false,maximizable:false,iconCls:'icon-save'" style="display:none;width:500px;height:400px;padding:10px;">
	 <!-- 实际内容 -->	 
			<form id="ff" method="post">
			    <!-- 主键 -->
			    <div style="margin-bottom: 20px;display:none;">
					<input class="easyui-textbox" prompt="系统编码" name="pid" id="pid"
						style="width: 100%; height: 34px;"
						data-options="label:'系统编码',labelPosition:'left',required:false">
				</div>
				<!--  -->
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="产品名称" name="pname" id="pname"
						style="width: 100%; height: 34px;"
						data-options="label:'产品名称',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="产品编码" name="pcode" id="pcode"
						style="width: 100%; height: 34px;"
						data-options="label:'产品编码',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="产品描述" name="pdesc" id="pdesc"
						style="width: 100%; height: 34px;" multiline="true"
						data-options="label:'产品描述',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="邮箱" name="email" id="email"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'邮箱',labelPosition:'left',required:true">
				</div>
					<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="手机" name="mobile" id="mobile"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'手机',labelPosition:'left',required:true">
				</div>			
			</form>
			<div style="text-align: center; padding: 5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="submitForm()" style="width: 80px">Submit</a> <a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="clearForm()" style="width: 80px">Clear</a>
			</div>		
	</div>
	
	
	
	
	
	
	<script type="text/javascript">
	<!--隐藏某一列-->
	// $(function(){
		 //$("#dg").datagrid("hideColumn", "role");
	// })
	//新增
	function submitForm(){
		var isValid = $("#ff").form('validate');
		if(false==isValid){
			return false;
		}
		//校验通过
		$.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/product/addOrUpdate',
				{
			        pid   : $('#pid').val(),
					pname : $('#pname').val(),
					pcode     : $('#pcode').val(),
					pdesc      : $('#pdesc').val(),
					email     : $('#email').val(),
					mobile     : $('#mobile').val()
				},
				function(result) {					
					$('#newWindow').window('close');//及时关闭
					if (result.success) {
						// 刷新用户数据  
						$('#dg')
								.datagrid(
										'reload');
					} else {
						$.messager
								.show({ // 显示错误信息  
									title : 'Error',
									msg : result.errorMsg
								});
					}
				}, 'json');
	}
	function clearForm() {
		$('#ff').form('clear');
	}	
	</script>
	
	<script type="text/javascript">			
		//删除
	    function destroy(){  
	        var row = $('#dg').datagrid('getSelected');  
	        if (row){  
	            $.messager.confirm('Confirm','确认要删除吗 ?',function(r){  
	                if (r){  
	                    $.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/product/del',
														{
															pid : row.pid
														},
														function(result) {
															if (result.success) {
																// 刷新用户数据  
																$('#dg')
																		.datagrid(
																				'reload');
															} else {
																$.messager
																		.show({ // 显示错误信息  
																			title : 'Error',
																			msg : result.errorMsg
																		});
															}
														}, 'json');
									}//if(r)结束
								}//function(r)结束
	            );//$.messager.confirm结束
			}//if(row)结束
		}//destroy结束 
	</script>

	
</body>

</html>