<%@ page language="java" session="false" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 网页布局参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/complextoolbar.html# -->
<!-- 添加一行，参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/rowediting.html -->
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/esf/esf.css">

<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>
<body style="padding: 0px 0px 0px 0px; margin: 1px 1px 1px 1px;">

<!-- 新增窗口 结束-->
	<script type="text/javascript">
		var toolbar = [{
			text:'新增',
			iconCls:'icon-add',
			handler:function(){
				clearForm();
				$("#serviceId").val("");
				$('#newWindow').window('open');
			}
		},{
			text:'修改',
			iconCls:'icon-edit',
			handler:function(){
				clearForm();
				var row = $('#dg').datagrid('getSelected');  
	       		if (row){ 
	 				//把相关的值补上	 				
	 				$('#ff').form('load',row);
	 				$('#newWindow').window('open');
	       		}	     		 
				
			}
		},'-',{
			text:'删除',
			iconCls:'icon-remove',
			handler:function(){destroy();}
		}];	
		</script>
		
	<table id="dg" title="" class="easyui-datagrid"
		style="width: 98%; height: auto"
		data-options="pagination:true,fitcolumns:true,rownumbers:true,singleSelect:true,url:'<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/serviceTemplate/query',method:'get',toolbar:toolbar,remoteSort:false,
                multiSort:true">
        <thead data-options="frozen:true">
            <tr>
                <th data-options="field:'id',width:40">ID</th>
				<th data-options="field:'serviceId',width:80">系统ID</th>				
				<th	data-options="field:'serviceName',width:100,align:'center'">服务名称</th>
				<th	data-options="field:'serviceCode',width:100,align:'center'">服务编码</th>				
				<th	data-options="field:'totalCount',width:100,align:'center'">总次数</th>
				<th	data-options="field:'thresholdCount',width:100,align:'center'">异常次数</th>
				<th	data-options="field:'alarmPeriod',width:100,align:'center'">报警周期(s)</th>	
            </tr>
        </thead>
        
		<thead>
			<tr>				
				<th	data-options="field:'succeedRatioThreshold',width:100,align:'center'">成功率(%)</th>
				<th	data-options="field:'averageTimeCostThreshold',width:100,align:'center'">平均耗时(ms)</th>
				<th	data-options="field:'maxTimeCostThreshold',width:100,align:'center'">最大耗时(ms)</th>
				<th	data-options="field:'minTimeCostThreshold',width:100,align:'center'">最小耗时(ms)</th>
				<th	data-options="field:'maxInvokedCount',width:100,align:'center'">最大次数</th>
				<th	data-options="field:'minInvokedCount',width:100,align:'center'">最小次数</th>
				<th data-options="field:'createTime',width:150,align:'center'">创建时间</th>
				<th data-options="field:'desc',width:400,align:'center'">描述</th>
				
			</tr>
		</thead>
	</table>
	
	
	
	
<!-- 新增/修改 窗口 开始 -->
	<div id="newWindow" class="easyui-window" title="编辑" data-options="modal:true,closed:true,minimizable:false,maximizable:false,iconCls:'icon-save'" style="display:none;width:500px;height:500px;padding:10px;">
	 <!-- 实际内容 -->	 
			<form id="ff" method="post">
			    <!-- 系统编码-->
			    <div style="margin-bottom: 20px;display:none;">
					<input class="easyui-textbox" prompt="系统编码" name="serviceId" id="serviceId"
						style="width: 100%; height: 34px;"
						data-options="label:'系统编码',labelPosition:'left',required:false">
				</div>
				<!--  -->
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="服务名称" name="serviceName" id="serviceName"
						style="width: 100%; height: 34px;"
						data-options="label:'服务名称',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="服务编码" name="serviceCode" id="serviceCode"
						style="width: 100%; height: 34px;"
						data-options="label:'服务编码',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="总次数" name="totalCount" id="totalCount"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'总次数',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="异常次数" name="thresholdCount" id="thresholdCount"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'异常次数',labelPosition:'left',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="报警周期" name="alarmPeriod" id="alarmPeriod"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'报警周期',labelPosition:'left',required:true">
				</div>	
				
				
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="成功率" name="succeedRatioThreshold" id="succeedRatioThreshold"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'成功率',labelPosition:'left',required:true">
				</div>
				
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="平均耗时" name="averageTimeCostThreshold" id="averageTimeCostThreshold"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'平均耗时',labelPosition:'left',required:true">
				</div>	
				
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="最大耗时" name="maxTimeCostThreshold" id="maxTimeCostThreshold"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'最大耗时',labelPosition:'left',required:true">
				</div>	
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="最小耗时" name="minTimeCostThreshold" id="minTimeCostThreshold"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'最小耗时',labelPosition:'left',required:true">
				</div>	
				
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="最大次数" name="maxInvokedCount" id="maxInvokedCount"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'最大次数',labelPosition:'left',required:true">
				</div>	
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="最小次数" name="minInvokedCount" id="minInvokedCount"
						style="width: 100%; height: 34px;" multiline="false"
						data-options="label:'最小次数',labelPosition:'left',required:true">
				</div>
				
				
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" prompt="描述" name="desc" id="desc"
						style="width: 100%; height: 60px;" multiline="true"
						data-options="label:'描述',labelPosition:'left',required:false">
				</div>
			</form>
			<div style="text-align: center; padding: 5px 0">
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="submitForm()" style="width: 80px">Submit</a> <a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="clearForm()" style="width: 80px">Clear</a>
			</div>		
	</div>
	
	
	
	
	
	
	<script type="text/javascript">
	<!--隐藏某一列-->
	// $(function(){
		 //$("#dg").datagrid("hideColumn", "role");
	// })
	//新增
	function submitForm(){
		var isValid = $("#ff").form('validate');
		if(false==isValid){
			return false;
		}
		//校验通过
		$.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/serviceTemplate/addOrUpdate',
				{
			        serviceId   : $('#serviceId').val(),
			        serviceName : $('#serviceName').val(),
			        serviceCode : $('#serviceCode').val(),
			        totalCount  : $('#totalCount').val(),
			        thresholdCount  : $('#thresholdCount').val(),
			        alarmPeriod     : $('#alarmPeriod').val(),
			        succeedRatioThreshold     : $('#succeedRatioThreshold').val(),
			        averageTimeCostThreshold  : $('#averageTimeCostThreshold').val(),
			        maxTimeCostThreshold      : $('#maxTimeCostThreshold').val(),
			        minTimeCostThreshold      : $('#minTimeCostThreshold').val(),
			        maxInvokedCount :$('#maxInvokedCount').val(),
			        minInvokedCount :$('#minInvokedCount').val(),
			        desc     : $('#desc').val()
				},
				function(result) {					
					$('#newWindow').window('close');//及时关闭
					if (result.success) {
						// 刷新用户数据  
						$('#dg')
								.datagrid(
										'reload');
					} else {
						$.messager
								.show({ // 显示错误信息  
									title : 'Error',
									msg : result.errorMsg
								});
					}
				}, 'json');
	}
	function clearForm() {
		$('#ff').form('clear');
	}	
	</script>
	
	<script type="text/javascript">			
		//删除
	    function destroy(){  
	        var row = $('#dg').datagrid('getSelected');  
	        if (row){  
	            $.messager.confirm('Confirm','确认要删除吗 ?',function(r){  
	                if (r){  
	                    $.post('<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/serviceTemplate/del',
														{
															serviceId : row.serviceId
														},
														function(result) {
															if (result.success) {
																// 刷新用户数据  
																$('#dg')
																		.datagrid(
																				'reload');
															} else {
																$.messager
																		.show({ // 显示错误信息  
																			title : 'Error',
																			msg : result.errorMsg
																		});
															}
														}, 'json');
									}//if(r)结束
								}//function(r)结束
	            );//$.messager.confirm结束
			}//if(row)结束
		}//destroy结束 
	</script>

	
</body>

</html>