<%@ page language="java" session="false"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="com.freedom.monitor.utils.ConstantUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- 网页布局参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/complextoolbar.html# -->
<!-- 添加一行，参考: file:///C:/Users/liuzhq/Desktop/jquery-easyui-1.5/demo/datagrid/rowediting.html -->
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/demo/demo.css">

<link rel="stylesheet" type="text/css"
	href="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/css/esf/esf.css">

<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.min.js"></script>
<script type="text/javascript"
	src="<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/js/jquery.easyui.min.js"></script>

</head>
<body style="padding: 0px 0px 0px 0px; margin: 1px 1px 1px 1px;">

	<!-- 产品查询条件
http://blog.csdn.net/heyangyi_19940703/article/details/52457934
 -->
<div id="tb" style="padding:2px 5px;">
		产品: <input id="productCodeComboBox" name="productCodeComboBox" class="easyui-combobox" style="width:150px"
			data-options="					
					url:'<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/productService/product',
					method:'get',
					valueField:'pcode',
					textField:'pname',
					panelHeight:'auto',
					label: '',
					labelPosition: 'left',
					editable:false
			"			>
		&nbsp;服务: <input id="serviceCodeCombobox"  name="serviceCodeCombobox"   class="easyui-combobox" style="width:150px"
			data-options="					
					url:'',
					method:'get',
					valueField:'scode',
					textField:'sname',
					panelHeight:'auto',
					label: '',
					labelPosition: 'left',
					editable:false
			"	
		>
		&nbsp;机器: <input id="serverIdCombobox" name="serverIdCombobox"  class="easyui-combobox" style="width:150px"
		data-options="					
					url:'',
					method:'get',
					valueField:'name',
					textField:'code',
					panelHeight:'auto',
					label: '',
					labelPosition: 'left',
					editable:false
			"	
		>
		
		&nbsp;<input id="subKeyTextBox" name="subKeyTextBox" class="easyui-textbox" style="width:300px" 
		  prompt="监控点模糊查询"
		  data-options="label:'',labelPosition:'left',required:false"
		  >
		  
		<a href="#" class="easyui-linkbutton" iconCls="icon-search">Search</a>
</div>






	<table id="dg" title="" class="easyui-datagrid"
		style="width: 98%; height: auto"
		data-options="pagination:true,fitcolumns:true,rownumbers:true,singleSelect:true,url:'',method:'get',toolbar:'#tb',remoteSort:false,
                multiSort:true">
		<thead data-options="">
			<tr>
				<th data-options="field:'id',width:40">ID</th>
				<th data-options="field:'subKey',width:80">监控点</th>
				<th data-options="field:'operation',width:100,align:'center'">操作</th>
			</tr>
		</thead>
	</table>

</body>
<script type="text/javascript">
<!--产品框动了-->

$(function () {
	//产品编码
    $('#productCodeComboBox').combobox({  	    	  
         onChange: function (newValue, oldValue) {  
       	  //0)长度校验
       	  var productCode = $("#productCodeComboBox").combobox("getValue");
       	  if(productCode.length<=0){
       		  return;
       	  }
       	  //1)清除所有的控件
       	  $('#serviceCodeCombobox').combobox('clear');
          $('#serverIdCombobox').combobox('clear');
          $("input[name='subKeyTextBox']").val("");
          $("#subKeyTextBox").val("");
          $('#dg').datagrid('loadData', { total: 0, rows: [] });  
       	  //2)刷新serviceCombobox   	
       	  var url='<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/data/service?productCode='+productCode;
       	  $('#serviceCodeCombobox').combobox('reload',url);       			  
       	  //onChange结束
         }
     });  
    //服务编码
    $('#serviceCodeCombobox').combobox({  	    	  
         onChange: function (newValue, oldValue) {  
       	  //0)长度校验
          var productCode = $("#productCodeComboBox").combobox("getValue");
          if(productCode.length<=0){
          		  return;
          }
       	  var serviceCode = $("#serviceCodeCombobox").combobox("getValue");
       	  if(serviceCode.length<=0){
       		  return;
       	  }
       	  //1)清除优先级低的控件
          $('#serverIdCombobox').combobox('clear');
          $("input[name='subKeyTextBox']").val("");
          $("#subKeyTextBox").val("");
          $('#dg').datagrid('loadData', { total: 0, rows: [] });  
       	  //2)刷新serviceCombobox   	
       	  var url='<%=request.getAttribute(ConstantUtils.GLOBAL_CONTEXT_PATH)%>/data/serverId?productCode='+productCode+'&serviceCode='+serviceCode;
       	  $('#serverIdCombobox').combobox('reload',url);       			  
       	  //onChange结束
         }
     });  
     
  //机器码变动
    $('#serverIdCombobox').combobox({  	    	  
         onChange: function (newValue, oldValue) {  
       	  //0)长度校验
          var serverIdCode = $("#serverIdCombobox").combobox("getValue");
          if(serverIdCode.length<=0){
          		  return;
          }
       	  //1)清除优先级低的控件
          $("input[name='subKeyTextBox']").val("");
          $("#subKeyTextBox").val("");  
          $('#dg').datagrid('loadData', { total: 0, rows: [] });         	      			  
       	  //onChange结束
         }
     });  
     
});  
</script>
</html>