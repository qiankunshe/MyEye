package com.freedom.monitor.myeye.storage.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.util.ResourceUtils;

import com.freedom.rpc.thrift.common.utils.Logger;

public class PropertyUtils {
	// logger
	private static final Logger logger = Logger.getLogger(PropertyUtils.class);

	// 私有方法，保证单例
	private PropertyUtils() {
	}

	//
	private static Properties myProperties = null;// 全局单例变量，一开始就存在
	static {// 静态块里，只加载一次
		Properties props = new Properties();
		try {
			InputStream in = new BufferedInputStream(
					new FileInputStream(ResourceUtils.getFile(CONFIG.CONFIG_FILE)));
			props.load(in);
			in.close();
		} catch (Exception e) {
			logger.error(e.toString());
			logger.error("fail to read config file " + CONFIG.CONFIG_FILE);
			System.exit(-1);
		}
		// 赋值
		logger.info("succeed to read config file " + CONFIG.CONFIG_FILE);
		myProperties = props;
		props = null;
		logger.info("succeed to create my config properties object ");
		// 结束
	}

	// 获取单例
	public static Properties getInstance() {
		return myProperties;
	}

	// 内部类
	static class CONFIG {
		public static String CONFIG_FILE = null;
		static {
			if (null != System.getProperty("configProperty")) {
				CONFIG_FILE = System.getProperty("configProperty");
			} else {
				try {
					if (ResourceUtils.getFile("classpath:server.properties").exists()) {
						CONFIG_FILE = "classpath:server.properties";
					} else {
						throw new Exception("server.properties not exist...");
					}
				} catch (Exception e) {// windows,eclipse
					CONFIG_FILE = "file:src/main/resources/server.properties";
				}
			}
			logger.info("config file --->"+CONFIG_FILE);
		}
	}

	public static void main(String[] args) {
		
	}
}

